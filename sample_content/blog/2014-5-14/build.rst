
Doing Stuff
###########

:tags: wings, fuselage, empennage, firewall-forward, panel, electrical, fabric, paint
:summary: Summary of doing stuff
:date: 2014-5-14


I did some stuff today. 

This is stuff
-------------

Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, 
quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo 
consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse 
cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat 
non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.

.. fig:: workshop-8.jpg
   
   This is a figure

And so stuff goes.
